<?php

use Illuminate\Database\Seeder;

use App\OrganizationStructure;
use App\OrganizationStructureInstance;
use App\Department;
use App\Job;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('organization_structures')->delete();
        
        OrganizationStructure::insert([
            [
                'name'          =>  'Foundation',
                'level'         =>  1,
                'created_at'    =>  new DateTime,
                'updated_at'    =>  new DateTime
            ],
            [
                'name'          =>  'University',
                'level'         =>  2,
                'created_at'    =>  new DateTime,
                'updated_at'    =>  new DateTime
            ],
            [
                'name'          =>  'Department',
                'level'         =>  3,
                'created_at'    =>  new DateTime,
                'updated_at'    =>  new DateTime
            ],
            [
                'name'          =>  'Position',
                'level'         =>  4,
                'created_at'    =>  new DateTime,
                'updated_at'    =>  new DateTime
            ]
        ]);
        
        //DB::table('organization_structure_instances')->delete();
        OrganizationStructureInstance::insert([
            [
                'organization_structure_id'             =>  '1',
                'name'                                  =>  'Yayasan Multimedia Nusantara',
                'organization_structure_instance_id'    =>  null,
                'created_at'                            =>  new DateTime,
                'updated_at'                            =>  new DateTime,
            ],

            [
                'organization_structure_id'             =>  '2',
                'name'                                  =>  'Universitas Multimedia Nusantara',
                'organization_structure_instance_id'    =>  1,
                'created_at'                            =>  new DateTime,
                'updated_at'                            =>  new DateTime,
            ],
        ]);

        OrganizationStructureInstance::where('id', '<>', '1')->where('id', '<>', '2')->withTrashed()->forceDelete();

        $department = Department::where('name', '<>', 'Rektorat')->with('department')->get();
        $rektor = new OrganizationStructureInstance;
        $rektor->organization_structure_id = 4;
        $rektor->name = 'Rektor';
        $rektor->organization_structure_instance_id = 2;
        $rektor->touch();
        $rektor->save();

        foreach ($department as $key => $value) {
            
            $parentDepartment = OrganizationStructureInstance::where('name', '=', $value->department->name)->get();

            $test = new OrganizationStructureInstance;
            $test->organization_structure_id = 3;
            $test->name = $value->name;

            if (count($parentDepartment) > 0) {

                $test->organization_structure_instance_id = $parentDepartment[0]->id;

            } else {

                $test->organization_structure_instance_id = 2;
                
            }
            
            $test->touch();
            $test->save();

            $job = Job::where('department_id', '=', $value->id)->where('multiple', '=', 0)->get();
            
            $position = new OrganizationStructureInstance;
            $position->organization_structure_id = 4;
            $position->name = $job[0]->name;
            $position->organization_structure_instance_id = $test->id;
            $position->touch();
            $position->save();
        }

        

        
    }
}
