<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('foundation_id')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('fax');
            $table->unique(['name', 'deleted_at']);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection('mysql_audit')->create('universities', function (Blueprint $table) {
            $table->integer('id')->unsigned()->primary();
            $table->integer('foundation_id')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('fax');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('universities');
        Schema::connection('mysql_audit')->dropIfExsists('universities');
    }
}
