<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePentahosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_pentaho')->create('pentahos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('form_name')->nullable();
            $table->string('form_no')->nullable();
            $table->string('standard')->nullable();
            $table->string('bagian')->nullable();
            $table->date('tgl_pengisian')->nullable();
            $table->integer('score')->nullable();
            $table->string('filename')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_pentaho')->drop('pentahos');
    }
}
