<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_batches', function(Blueprint $table) {
            $table->increments('id');
            $table->string('batch');
            $table->integer('work_id')->unsigned();
            $table->foreign('work_id')->references('id')->on('works')->onDelete('cascade');
            $table->datetime('expired_at');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection('mysql_audit')->create('task_batches', function(Blueprint $table) {
            $table->integer('id')->unsigned()->primary();
            $table->string('batch');
            $table->integer('work_id')->unsigned();
            $table->datetime('expired_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('task_batches');
        Schema::connection('mysql_audit')->dropIfExsists('task_batches');
    }
}
