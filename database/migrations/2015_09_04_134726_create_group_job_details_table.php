<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupJobDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_job_details', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('group_job_id')->unsigned();
            $table->foreign('group_job_id')->references('id')->on('group_jobs')->onDelete('cascade');
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->unique(['group_job_id', 'job_id', 'deleted_at']);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection('mysql_audit')->create('group_job_details', function(Blueprint $table) {
            $table->integer('id')->unsigned()->primary();
            $table->integer('group_job_id')->unsigned();
            $table->integer('job_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('group_job_details');
        Schema::connection('mysql_audit')->dropIfExsists('group_job_details');
    }
}
