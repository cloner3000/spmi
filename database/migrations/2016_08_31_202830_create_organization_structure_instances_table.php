<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationStructureInstancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_structure_instances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organization_structure_id')->unsigned();
            $table->foreign('organization_structure_id', 'osi_os')->references('id')->on('organization_structures')->onDelete('cascade');
            $table->string('name');
            $table->integer('organization_structure_instance_id')->unsigned()->nullable();
            $table->foreign('organization_structure_instance_id', 'osi_osi')->references('id')->on('organization_structure_instances')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organization_structure_instances');
    }
}
