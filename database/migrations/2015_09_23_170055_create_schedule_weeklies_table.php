<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleWeekliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_weeklies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('day_start');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection('mysql_audit')->create('schedule_weeklies', function (Blueprint $table) {
            $table->integer('id')->unsigned()->primary();
            $table->integer('day_start');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schedule_weeklies');
        Schema::connection('mysql_audit')->dropIfExsists('schedule_weeklies');
    }
}
