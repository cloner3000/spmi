<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentStandardMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_standard_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_standard_id')->unsigned()->nullable();
            $table->foreign('document_standard_id', 'ds_dsm')->references('id')->on('document_standards')->onDelete('cascade');
            $table->string('no')->nullable();
            $table->datetime('date');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('filename')->nullable();
            $table->integer('document_standard_master_id')->unsigned()->nullable();
            $table->foreign('document_standard_master_id', 'dsm_dsmid')->references('id')->on('document_standard_masters')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('document_standard_masters');
    }
}
