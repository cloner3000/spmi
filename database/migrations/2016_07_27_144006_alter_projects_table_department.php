<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectsTableDepartment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function($table) {
            $table->integer('department_id')->unsigned()->nullable()->after('user_id')->default(null);
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });

        Schema::connection('mysql_audit')->table('projects', function($table) {
            $table->integer('department_id')->unsigned()->nullable()->after('user_id')->default(null);
            //$table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
