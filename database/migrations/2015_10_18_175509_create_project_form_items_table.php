<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFormItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_form_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_form_id')->unsigned();
            $table->foreign('project_form_id')->references('id')->on('project_forms')->onDelete('cascade');
            $table->integer('form_id')->unsigned();
            $table->foreign('form_id')->references('id')->on('forms')->onDelete('cascade');
            $table->string('document');
            $table->unique(['project_form_id', 'form_id', 'deleted_at']);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection('mysql_audit')->create('project_form_items', function (Blueprint $table) {
            $table->integer('id')->unsigned()->primary();
            $table->integer('project_form_id')->unsigned();
            $table->integer('form_id')->unsigned();
            $table->string('document');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_form_items');
        Schema::connection('mysql_audit')->dropIfExsists('project_form_items');
    }
}
