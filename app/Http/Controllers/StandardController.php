<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Standard;
use App\StandardDocument;
use App\Guide;
use App\Instruction;
use App\Form;


use App\DocumentStandard;
use App\DocumentStandardMaster;

use Response;
use Auth;

use DateTime;

class StandardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public function index()
    {
        return Standard::get();
    }

    public function paginate(Request $request, $display = 10)
    {
        return Standard::paginate($display);
    }

	public function all() {
		$standard = Standard::with('standardDocuments.guides.instructions.forms')->get();
		return Response::json($standard, 200, [], JSON_PRETTY_PRINT);
		
	}
    
    public function combination(Request $request) 
    {
        $display = $request->display;
        $withStandardDocument = $request->standardDocument;
        $withGuide = $request->guide;
        $withInstruction = $request->instruction;
        $withForm = $request->form;
        $keyword = rawurldecode($request->keyword);
        $response = [];
        
        if ($withStandardDocument == 'true') {
            $standardDocument = StandardDocument::where('description', 'LIKE', '%' . $keyword . '%')->orWhere('no', 'LIKE', '%' . $keyword . '%')->get();
            foreach ($standardDocument as $key => $value) {
                
                $value->type = "s";
                array_push($response, $value);
            }
            
        }
        
        if ($withGuide == 'true') {
            $guide = Guide::where('description', 'LIKE', '%' . $keyword . '%')->orWhere('no', 'LIKE', '%' . $keyword . '%')->get();
            foreach($guide as $key => $value) {
                
                $value->type = "g";
                array_push($response, $value);
                
            }
        }
        
        if ($withInstruction == 'true') {
            $instruction = Instruction::where('description', 'LIKE', '%' . $keyword . '%')->orWhere('no', 'LIKE', '%' . $keyword . '%')->get();
            foreach($instruction as $key => $value) {
                
                $value->type = "i";
                array_push($response, $value);
                
            }
        }
        
        if ($withForm == 'true') {
            $form = Form::where('description', 'LIKE', '%' . $keyword . '%')->orWhere('no', 'LIKE', '%' . $keyword . '%')->get();
            foreach($form as $key => $value) {
                
                $value->type = "f";
                array_push($response, $value);
                
            }
        } 
        
        //return $request->query()['page'];
        $page = $request->query()['page'];
        
        $slice = array_slice($response, $display * ($page - 1), $display);
        
        $paginator = new Paginator($slice, count($response), $display, $page, [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]);
        
        return $paginator;
        
        //return response()->json($response);
        
    }
	
    public function store(Request $request)
    {   
        if (Auth::check()) {
            $standard = new Standard;
            $standard->description = $request->input('description');
            $standard->date = $request->input('date');
            $standard->touch();
            $standard->save(); 
        } else {
            return Response::json(['title' => 'Error', 'message' => 'Authentication failed'], 403);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        return Standard::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $standard = Standard::find($id);
        $standard->description = $request->input('description');
        $standard->date = $request->input('date');
        $standard->touch();
        $standard->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $standard = Standard::find($id);
        $standard->delete();
    }

    public function validating(Request $request)
    {
        if ($request->input('id')) {
            return Standard::where('description', '=', $request->input('description'))
                ->where('id', '<>', $request->input('id'))
                ->get();
        } else {
            return Standard::where('description', '=', $request->input('description'))
                ->get();    
        }
    }

    public function migrate() 
    {
        /*
        DocumentStandard::withTrashed()->forceDelete();
        DocumentStandard::insert([
            [
                'id'            =>  1,   
                'name'          =>  'Kumpulan Standar',
                'level'         =>  '1',
                'created_at'    =>  new DateTime,
                'updated_at'    =>  new DateTime,
            ],

            [
                'id'            =>  2,
                'name'          =>  'Dokumen Standar Mutu',
                'level'         =>  '2',
                'created_at'    =>  new DateTime,
                'updated_at'    =>  new DateTime,
            ],

            [
                'id'            =>  3,
                'name'          =>  'Dokumen Pedoman Mutu',
                'level'         =>  '3',
                'created_at'    =>  new DateTime,
                'updated_at'    =>  new DateTime,
            ],

            [
                'id'            =>  4,
                'name'          =>  'Dokumen Instruksi Kerja',
                'level'         =>  '4',
                'created_at'    =>  new DateTime,
                'updated_at'    =>  new DateTime,
            ],

            [
                'id'            =>  5,
                'name'          =>  'Dokumen Form',
                'level'         =>  '5',
                'created_at'    =>  new DateTime,
                'updated_at'    =>  new DateTime,
            ],
        ]);
        */

        DocumentStandardMaster::withTrashed()->forceDelete();
        $kumpulan = Standard::get();

        foreach ($kumpulan as $key => $value) {

            
            $dsm = new DocumentStandardMaster;
            $dsm->document_standard_id  =  1;
            $dsm->no                    =  substr($value->description, 0, 3);
            $dsm->date                  =  $value->date;
            $dsm->name                  =  substr($value->description, 6);
            $dsm->created_at            =  $value->created_at;
            $dsm->updated_at            =  $value->updated_at;
            $dsm->save();
        }

        //return 'berhasil';

        $standardDocument = StandardDocument::with('standard')->get();

        foreach($standardDocument as $key => $value) {

            $std = DocumentStandardMaster::where('no', '=', substr($value->standard->description, 0, 3))->get();

            $dsm = new DocumentStandardMaster;
            $dsm->document_standard_id          =  2;
            switch (count($value->no)) {
                case 5 : 
                $dsm->no                        =  $value->no;
                break;

                default : 
                $dsm->no                        =  $value->no;
                break;
            }
            
            $dsm->date                          =  $value->date;
            $dsm->name                          =  $value->description;
            $dsm->filename                      =  $value->document;
            $dsm->document_standard_master_id   =  $std[0]->id;
            $dsm->created_at                    =  $value->created_at;
            $dsm->updated_at                    =  $value->updated_at;

            $dsm->save();
        }





    }
}
