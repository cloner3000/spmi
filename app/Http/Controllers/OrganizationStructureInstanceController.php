<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Job;
use App\OrganizationStructure;
use App\OrganizationStructureInstance;
use App\User;
use App\UserPosition;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Libraries\SOP;





class OrganizationStructureInstanceController extends Controller
{

    private function snakes ($string) {
        
        return Str::snake(Str::plural($string));
    }

    private function recursiveSelect($instance) {

        foreach ($instance as $key => $value) {

            $sub = OrganizationStructureInstance::with('level')->with('users')
                ->where('organization_structure_instance_id', $value->id)->get();
            $sub = json_decode(json_encode($sub), false);

            $instance[$key]->level = $value->level->name;

            if (count($sub) > 0) {

                $level = [];
                //checking each sub
                foreach($sub as $subKey => $subValue) {
                    if (!in_array($subValue->level->name, $level)) {
                        array_push($level, $subValue->level->name);
                    }     
                }

                $i = 0;
                for($i = 0; $i < count($level); $i++) {
                    $instance[$key]->{$this->snakes($level[$i])} = [];    
                }

                foreach($sub as $subKey => $subValue) {
                    foreach($instance[$key] as $instanceKey => $instanceValue) {
                        if($instanceKey == $this->snakes($subValue->level->name)) {
                            array_push($instance[$key]->{$this->snakes($instanceKey)}, $subValue);
                        }
                    }
                }

                $this->recursiveSelect($sub);
        
            }
            

            
        }
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $instance = OrganizationStructureInstance::with('level')->where('organization_structure_id', '1')->get();
        $instance = json_decode(json_encode($instance), false);

      

        $this->recursiveSelect($instance);
        return response()->json($instance);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $instance = new OrganizationStructureInstance;
        $instance->organization_structure_id = $request->input('organization_structure_id');
        $instance->name = $request->input('name');
        $instance->touch();
        $instance->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Migrate from hierarchy to organization 
     *
     * @return \Illuminate\Http\Response
     */
    public function migrate()
    {

        OrganizationStructureInstance::where('id', '<>', '1')->where('id', '<>', '2')->withTrashed()->forceDelete();

        $department = Department::where('name', '<>', 'Rektorat')->with('department')->get();
        $rektor = new OrganizationStructureInstance;
        $rektor->organization_structure_id = 4;
        $rektor->name = 'Rektor';
        $rektor->organization_structure_instance_id = 2;
        $rektor->touch();
        $rektor->save();

        foreach ($department as $key => $value) {
            
            $parentDepartment = OrganizationStructureInstance::where('name', '=', $value->department->name)->get();

            $test = new OrganizationStructureInstance;
            $test->organization_structure_id = 3;
            $test->name = $value->name;

            if (count($parentDepartment) > 0) {

                $test->organization_structure_instance_id = $parentDepartment[0]->id;

            } else {

                $test->organization_structure_instance_id = 2;
                
            }
            
            $test->touch();
            $test->save();

            $job = Job::where('department_id', '=', $value->id)->where('multiple', '=', 0)->get();
            
            $position = new OrganizationStructureInstance;
            $position->organization_structure_id = 4;
            $position->name = $job[0]->name;
            $position->organization_structure_instance_id = $test->id;
            $position->touch();
            $position->save();
        }

        UserPosition::withTrashed()->forceDelete();

        $users = User::with('jobs')->get();
        //return response()->json($users);

        $users = json_decode(json_encode($users), false);
        
        foreach($users as $userKey => $userValue) {

            foreach($userValue->jobs as $userJobKey => $userJobValue) {

                $position = OrganizationStructureInstance::where('name', '=', $userJobValue->name)->first();
                
                //return response()->json($position);
                if ($position) {
                    $userPosition = new UserPosition;
                    $userPosition->user_id = $userValue->id;
                    $userPosition->position_id = $position->id;
                    $userPosition->touch();
                    $userPosition->save();
                }
                    
                
            }
            
        }

        return response()->json($users);
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
