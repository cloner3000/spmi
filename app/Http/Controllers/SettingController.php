<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use App\User;
use App\Standard;
use App\StandardDocument;
use App\Guide;
use App\Instruction;
use App\Form;
use App\University;
use App\Department;
use App\Job;
use App\UserJob;
use App\GroupJob;
use App\GroupJobDetail;
use App\Work;
use App\WorkForm;
use App\TaskBatch;
use App\Task;
use App\Semester;
use App\ScheduleDaily;
use App\ScheduleWeekly;
use App\ScheduleMonthly;
use App\ScheduleSemesterly;
use App\ScheduleYearly;
use App\ScheduleDailyDay;
use App\Project;
use App\ProjectNode;
use App\ProjectNodeDelegation;
use App\ProjectForm;
use App\ProjectUser;
use App\ProjectFormItem;
use App\ProjectFormUpload;
use App\UserRegistration;
use App\ProjectAssessor;
use App\ProjectFormScore;
use App\ProjectNodeAssessor;
use App\ProjectTemplate;
use App\ProjectNodeTemplate;
use App\ProjectNodeFormTemplate;
use App\ProjectNodeFormItemTemplate;
use App\ProjectFormUploadAttachment;
use App\HistoryDownload;
use App\PhysicalAddressCategory;
use App\PhysicalAddress;
use App\AssignmentTemplate;
use App\AssignmentAttachmentTemplate;
use App\AssignmentRecipient;
use App\AssignmentDelegation;
use App\AssignmentUpload;

class SettingController extends Controller
{
    /**
     * For cleaning soft delete data;
     *
     * @return \Illuminate\Http\Response
     */
    public function clean()
    {
       
        $report = new \stdClass;

        $assignment_upload = AssignmentUpload::onlyTrashed()->get();
        $assignment_delegation = AssignmentDelegation::onlyTrashed()->get();
        $assignment_recipient = AssignmentRecipient::onlyTrashed()->get();
        $assignment_attachment_template = AssignmentAttachmentTemplate::onlyTrashed()->get();
        $assignment_template = AssignmentTemplate::onlyTrashed()->get();
        $physical_address = PhysicalAddress::onlyTrashed()->get();
        $physical_address_category = PhysicalAddressCategory::onlyTrashed()->get();
        $history_download = HistoryDownload::onlyTrashed()->get();


        $user = User::onlyTrashed()->get();
        $user_trashed = json_decode(json_encode($user), true);
        $user_audit = DB::connection('mysql_audit')->table('users')->insert($user_trashed);

        $standard = Standard::onlyTrashed()->get();
        $standard_trashed = json_decode(json_encode($standard), true);
        DB::connection('mysql_audit')->table('standards')->insert($standard_trashed);
        
        
        $standard_document = StandardDocument::onlyTrashed()->get();
        $standard_document_trashed = json_decode(json_encode($standard_document), true);
        DB::connection('mysql_audit')->table('standard_documents')->insert($standard_document_trashed);

        $guide = Guide::onlyTrashed()->get();
        $guide_trashed = json_decode(json_encode($guide), true);
        DB::connection('mysql_audit')->table('guides')->insert($guide_trashed);

        $instruction = Instruction::onlyTrashed()->get();
        $instruction_trashed = json_decode(json_encode($instruction), true);
        DB::connection('mysql_audit')->table('instructions')->insert($instruction_trashed);

        
        $form = Form::onlyTrashed()->get();
        $form_trashed = json_decode(json_encode($form), true);
        DB::connection('mysql_audit')->table('forms')->insert($form_trashed);
        
        $university = University::onlyTrashed()->get();
        $university_trashed = json_decode(json_encode($university), true);
        DB::connection('mysql_audit')->table('universities')->insert($university_trashed);

        $department = Department::onlyTrashed()->get();
        $department_trashed = json_decode(json_encode($department), true);
        DB::connection('mysql_audit')->table('departments')->insert($department_trashed);

        $job = Job::onlyTrashed()->get();
        $job_trashed = json_decode(json_encode($job), true);
        DB::connection('mysql_audit')->table('jobs')->insert($job_trashed);

        $userJob = UserJob::onlyTrashed()->get();
        $userJob_trashed = json_decode(json_encode($userJob), true);
        DB::connection('mysql_audit')->table('user_jobs')->insert($userJob_trashed);

        $groupJob = GroupJob::onlyTrashed()->get();
        $groupJob_trashed = json_decode(json_encode($groupJob), true);
        DB::connection('mysql_audit')->table('group_jobs')->insert($groupJob_trashed);

        
        $groupJobDetail = GroupJobDetail::onlyTrashed()->get();
        $groupJobDetail_trashed = json_decode(json_encode($groupJobDetail), true);
        DB::connection('mysql_audit')->table('group_job_details')->insert($groupJobDetail_trashed);

        $work = Work::onlyTrashed()->get();
        $work_trashed = json_decode(json_encode($work), true);
        DB::connection('mysql_audit')->table('works')->insert($work_trashed);

        $workForm = WorkForm::onlyTrashed()->get();
        $workForm_trashed = json_decode(json_encode($workForm), true);
        DB::connection('mysql_audit')->table('work_forms')->insert($workForm_trashed);

        $taskBatch = TaskBatch::onlyTrashed()->get();
        $taskBatch_trashed = json_decode(json_encode($taskBatch), true);
        DB::connection('mysql_audit')->table('task_batches')->insert($taskBatch_trashed);

        
        $task = Task::onlyTrashed()->get();
        $task_trashed = json_decode(json_encode($task), true);
        DB::connection('mysql_audit')->table('tasks')->insert($task_trashed);
        
        $semester = Semester::onlyTrashed()->get();
        $semester_trashed = json_decode(json_encode($semester), true);
        DB::connection('mysql_audit')->table('semesters')->insert($semester_trashed);

        $scheduleDaily = ScheduleDaily::onlyTrashed()->get();
        $scheduleDaily_trashed = json_decode(json_encode($scheduleDaily), true);
        DB::connection('mysql_audit')->table('schedule_dailies')->insert($scheduleDaily_trashed);

        $scheduleWeekly = ScheduleWeekly::onlyTrashed()->get();
        $scheduleWeekly_trashed = json_decode(json_encode($semester), true);
        DB::connection('mysql_audit')->table('schedule_weeklies')->insert($scheduleWeekly_trashed);

        $scheduleMonthly = ScheduleMonthly::onlyTrashed()->get();
        $scheduleMonthly_trashed = json_decode(json_encode($scheduleMonthly), true);
        DB::connection('mysql_audit')->table('schedule_monthlies')->insert($scheduleMonthly_trashed);

        $scheduleSemesterly = ScheduleSemesterly::onlyTrashed()->get();
        $scheduleSemesterly_trashed = json_decode(json_encode($scheduleSemesterly), true);
        DB::connection('mysql_audit')->table('schedule_semesterlies')->insert($scheduleSemesterly_trashed);

        
        $scheduleYearly = ScheduleYearly::onlyTrashed()->get();
        $scheduleYearly_trashed = json_decode(json_encode($scheduleYearly), true);
        DB::connection('mysql_audit')->table('schedule_yearlies')->insert($scheduleYearly_trashed);

        $scheduleDailyDay = ScheduleDailyDay::onlyTrashed()->get();
        $scheduleDailyDay_trashed = json_decode(json_encode($scheduleDailyDay), true);
        DB::connection('mysql_audit')->table('schedule_daily_days')->insert($scheduleDailyDay_trashed);

        $project = Project::onlyTrashed()->get();
        $project_trashed = json_decode(json_encode($project), true);
        DB::connection('mysql_audit')->table('projects')->insert($project_trashed);

        $projectNode = ProjectNode::onlyTrashed()->get();
        $projectNode_trashed = json_decode(json_encode($projectNode), true);
        DB::connection('mysql_audit')->table('project_nodes')->insert($projectNode_trashed);

        $projectNodeDelegation = ProjectNodeDelegation::onlyTrashed()->get();
        $projectNodeDelegation_trashed = json_decode(json_encode($projectNodeDelegation), true);
        DB::connection('mysql_audit')->table('project_node_delegations')->insert($projectNodeDelegation_trashed);
        
        
        $projectForm = ProjectForm::onlyTrashed()->get();
        $projectForm_trashed = json_decode(json_encode($projectForm), true);
        DB::connection('mysql_audit')->table('project_forms')->insert($projectForm_trashed);

        $projectUser = ProjectUser::onlyTrashed()->get();
        $projectUser_trashed = json_decode(json_encode($projectUser), true);
        DB::connection('mysql_audit')->table('project_users')->insert($projectUser_trashed);

        $projectFormItem = ProjectFormItem::onlyTrashed()->get();
        $projectFormItem_trashed = json_decode(json_encode($projectFormItem), true);
        DB::connection('mysql_audit')->table('project_form_items')->insert($projectFormItem_trashed);

        
        $projectFormUpload = ProjectFormUpload::onlyTrashed()->get();
        $projectFormUpload_trashed = json_decode(json_encode($projectFormUpload), true);
        DB::connection('mysql_audit')->table('project_form_uploads')->insert($projectFormUpload_trashed);

        $projectAssessor = ProjectAssessor::onlyTrashed()->get();
        $projectAssessor_trashed = json_decode(json_encode($projectAssessor), true);
        DB::connection('mysql_audit')->table('project_assessors')->insert($projectAssessor_trashed);

        
        
        $projectFormScore = ProjectFormScore::onlyTrashed()->get();
        $projectFormScore_trashed = json_decode(json_encode($projectFormScore), true);
        DB::connection('mysql_audit')->table('project_form_scores')->insert($projectFormScore_trashed);

        
        $projectNodeAssessor = ProjectNodeAssessor::onlyTrashed()->get();
        $projectNodeAssessor_trashed = json_decode(json_encode($projectNodeAssessor), true);
        DB::connection('mysql_audit')->table('project_node_assessors')->insert($projectNodeAssessor_trashed);

        $projectTemplate = ProjectTemplate::onlyTrashed()->get();
        $projectTemplate_trashed = json_decode(json_encode($projectTemplate), true);
        DB::connection('mysql_audit')->table('project_templates')->insert($projectTemplate_trashed);

        
        
        DB::connection('mysql_audit')->table(
            'project_node_templates'
        )->insert(json_decode(json_encode(
            ProjectNodeTemplate
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'project_node_form_templates'
        )->insert(json_decode(json_encode(
            ProjectNodeFormTemplate
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'project_node_form_item_templates'
        )->insert(json_decode(json_encode(
            ProjectNodeFormItemTemplate
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'project_form_upload_attachments'
        )->insert(json_decode(json_encode(
            ProjectFormUploadAttachment
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'physical_address_categories'
        )->insert(json_decode(json_encode(
            PhysicalAddressCategory
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'physical_addresses'
        )->insert(json_decode(json_encode(
            PhysicalAddress
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'assignment_templates'
        )->insert(json_decode(json_encode(
            AssignmentTemplate
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'assignment_attachment_templates'
        )->insert(json_decode(json_encode(
            AssignmentAttachmentTemplate
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'assignment_attachment_recipients'
        )->insert(json_decode(json_encode(
            AssignmentRecipient
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'assignment_delegations'
        )->insert(json_decode(json_encode(
            AssignmentDelegation
        ::onlyTrashed()->get()), true));

        DB::connection('mysql_audit')->table(
            'assignment_uploads'
        )->insert(json_decode(json_encode(
            AssignmentUpload
        ::onlyTrashed()->get()), true));

        
        
        
        AssignmentUpload::onlyTrashed()->forceDelete();
        AssignmentDelegation::onlyTrashed()->forceDelete();
        AssignmentRecipient::onlyTrashed()->forceDelete();
        AssignmentAttachmentTemplate::onlyTrashed()->forceDelete();
        AssignmentTemplate::onlyTrashed()->forceDelete();
        PhysicalAddress::onlyTrashed()->forceDelete();
        
        PhysicalAddressCategory::onlyTrashed()->forceDelete();
        ProjectFormUploadAttachment::onlyTrashed()->forceDelete();

        
        ProjectNodeFormItemTemplate::onlyTrashed()->forceDelete();
        ProjectNodeFormTemplate::onlyTrashed()->forceDelete();
        ProjectNodeTemplate::onlyTrashed()->forceDelete();
        
        ProjectTemplate::onlyTrashed()->forceDelete();
        ProjectNodeAssessor::onlyTrashed()->forceDelete();
        ProjectFormScore::onlyTrashed()->forceDelete();
        ProjectAssessor::onlyTrashed()->forceDelete();
        
        ProjectFormUpload::onlyTrashed()->forceDelete();
        ProjectFormItem::onlyTrashed()->forceDelete();
        ProjectUser::onlyTrashed()->forceDelete();
        ProjectForm::onlyTrashed()->forceDelete();
        ProjectNodeDelegation::onlyTrashed()->forceDelete();
        
        ProjectNode::onlyTrashed()->forceDelete();
        Project::onlyTrashed()->forceDelete();
        ScheduleDailyDay::onlyTrashed()->forceDelete();
        ScheduleYearly::onlyTrashed()->forceDelete();
        ScheduleSemesterly::onlyTrashed()->forceDelete();
        ScheduleMonthly::onlyTrashed()->forceDelete();
        

        ScheduleWeekly::onlyTrashed()->forceDelete();
        ScheduleDaily::onlyTrashed()->forceDelete();
        Semester::onlyTrashed()->forceDelete();	
        Task::onlyTrashed()->forceDelete();
        
        TaskBatch::onlyTrashed()->forceDelete();
        
        WorkForm::onlyTrashed()->forceDelete();
        
        Work::onlyTrashed()->forceDelete();
        GroupJob::onlyTrashed()->forceDelete();
        UserJob::onlyTrashed()->forceDelete();
        Job::onlyTrashed()->forceDelete();
        Department::onlyTrashed()->forceDelete();
        University::onlyTrashed()->forceDelete();
        Form::onlyTrashed()->forceDelete();
        Instruction::onlyTrashed()->forceDelete();
        Guide::onlyTrashed()->forceDelete();
        StandardDocument::onlyTrashed()->forceDelete();
        
        Standard::onlyTrashed()->forceDelete();
        
        User::onlyTrashed()->forceDelete();
        
        return response()->json($report);
    }
}
