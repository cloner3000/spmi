<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Guide;
use DB;

class GuideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, $display)
    {
        $keyword = rawurldecode($request->keyword);
        $guide = Guide::where('no', 'LIKE', '%' . $keyword . '%')
            ->orWhere(function($query) use ($keyword) {
                $query->whereNull('deleted_at')->where('description', 'LIKE', '%' . $keyword . '%');
            })
            ->orWhere(function($query) use ($keyword) {
                $query->whereNull('deleted_at')->whereHas('standardDocument', function($query1) use ($keyword) {
                    $query1->where('description', 'LIKE', '%' . $keyword . '%');
                });
        })->with('standardDocument.standard')->paginate($display);
        return response()->json($guide);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        

        $guide = new Guide;
        $guide->standard_document_id = $request->input('standard_document_id');
        $guide->no = $request->input('no');
        $guide->date = $request->input('date');
        $guide->description = $request->input('description');
        $guide->document = $request->input('filename');

        $guide->touch();
        $guide->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Guide::with('StandardDocument.standard')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $guide = Guide::find($id);
        $guide->standard_document_id = $request->input('standard_document_id');
        $guide->no = $request->input('no');
        $guide->date = $request->input('date');
        $guide->description = $request->input('description');

        if ($request->input('filename')) {
            $guide->document = $request->input('filename');
        }

        $guide->touch();
        $guide->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $guide = Guide::find($id);
        $guide->delete();
    }

    public function standarddocument ($id) {
        return Guide::with('StandardDocument.standard')->where('standard_document_id', '=', $id)->get();
    }

    public function validatingNo(Request $request)
    {
        if ($request->input('id')) {
            return Guide::where('no', '=', $request->input('no'))
                ->where('id', '<>', $request->input('id'))
                ->get();
        } else {
            return Guide::where('no', '=', $request->input('no'))
                ->get();    
        }
    } 

    public function validatingDescription(Request $request)
    {
        if ($request->input('id')) {
            return Guide::where('description', '=', $request->input('description'))
                ->where('id', '<>', $request->input('id'))
                ->get();
        } else {
            return Guide::where('description', '=', $request->input('description'))
                ->get();    
        }
    }
}
