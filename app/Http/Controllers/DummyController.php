<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;

use App\Project;
use App\ProjectNode;

use App\ProjectForm;
use App\ProjectFormItem;
use App\Form;
use App\ProjectFormUpload;


use App\Pica;
use App\PicaDetail;

use App\userJob;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class DummyController extends Controller
{

    //use ProjectTrait;

    private $projectPica = [];
    private $endNodes = [];

    private function selectEndNode($nodes) {

        foreach ($nodes as $nodeKey => $nodeValue) {

            $projectNode = ProjectNode::where('project_id', '=', $nodeValue->id)->with('pica.picaDetails')->with(['forms' => function($query) {
                $query->with('indicators')->with('score');
            }])->where('project_type', '<>', 'App\Project')->get();

            if (count($projectNode) > 0) {

                $this->selectEndNode($projectNode);

            } else {

                array_push($this->endNodes, $nodeValue);

            }

        }
    }

    public function selectNodeAll($nodes, $parent, $id) {

    	 foreach($nodes as $key => $value) {

            $projectNode = ProjectNode::with('pica.picaDetails')->where('project_id', '=', $value->id)->where('project_type', '<>', 'App\Project')->get();

            if (count($projectNode) > 0) {

              $nodes[$key]['children'] = $projectNode;
              $this->selectNodeAll($projectNode, $nodes[$key], $id);

            } else {

                if (count($value->pica) > 0) {
                    $projectForm = ProjectForm::where('project_node_id', '=', $value->id)->with('score', 'indicators')->first();
                    $nodes[$key]['project_forms'] = $projectForm;
                    
                    if ($value->pica->user_id == $id) {
                        array_push($this->projectPica, $nodes[$key]);
                    } else {
                        $node = $nodes[$key];
                        $node = json_decode(json_encode($node), true);//decode
                        
                        if ($node['pica']['user_id'] == $id) { //check apakah user itu sama dengan penangung jawab pica
                            array_push($this->projectPica, $node);  //push beserta detailnya untuk direturn
                        } else { // bila tidak, check apabila user tersebut menjadi PIC di detail pica
                            $detail = $node['pica']['pica_details']; //
                            unset($node['pica']['pica_details']); //remove pica detail untuk sementar
                            $newDetail = []; //container pica, untuk menampung detail bila user sebagai PIC
                            foreach ($detail as $keyDetail => $valueDetail) { // loop untuk check masing2 detail dengan pica
                                if($valueDetail['user_id'] == $id) { //check if id PIC sama dengan user yang sedang login
                                    array_push($newDetail, $valueDetail); //apabila sama masukan detail kedalam detail sementar
                                }
                            }
                            $node['pica']['pica_details'] = $newDetail; //kembalikan detail sementara kedalam attribut pica
                            if (count($newDetail) == 0) { //bila tidak ada detail sama sekali, maka pica tidak perlu ditampilkan
                                $node['pica'] = null;// menhapus pica
                            }
                            array_push($this->projectPica, $node); //masukan node kedalam project pica untuk direturn;
                        }

                    }
                    
                }
                
            }
        }
    }

    /**
     * To Display rencana perbaikan;
     *0
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
      $user = new \stdClass;
      $user->id = 1;

       //$project = Project::with('projects.projects.projects.projects')->where('status', '<>', 0)->whereNotNull('department_id')->get();
        $project = Project::with('projects')->where('status', '<>', 0)->whereNotNull('department_id')->get();
        foreach ($project as $key => $value) {
            $this->selectNodeAll($value->projects, $value, $user->id);
            $value->nodes = $this->projectPica;
            $this->projectPica = [];
            unset($value->projects);
        }

        $filteredPica = [];

        foreach ($project as $projectKey => $projectValue) {
            if (count($projectValue->nodes) > 0) {
                array_push($filteredPica, $projectValue);
            }
        }
     //print_r ($value);
        //return response()->json($this->projectPica);
        return response()->json($filteredPica);
    }

    /**
     * To display display all standard with indicators,
     */

    public function visualize($id) {
        
        /*
        $projects = Project::with('projects')->where('status', '<>', 0)->whereNotNull('department_id')->get();
        foreach ($projects as $key => $value) {
            $this->selectEndNode($value->projects);
            $value->nodes = $this->endNodes;
            $this->endNodes = [];
            unset($value->projects);
        }
        */

        $project = Project::with('projects')->where('status', '<>', 0)->whereNotNull('department_id')->find($id);
        $this->selectEndNode($project->projects);
        $project->nodes = $this->endNodes;
        $this->endNodes = [];
        unset($project->projects);

        return response()->json($project);
    }

    /*public function assessment ($id) 
    {
      $project = Project::with('projects')->find($id);
      $this->selectNodeAll($project->projects, $project, 1);

      return response()->json($this->projectPica);
    }*/ 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
