<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationStructureInstance extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['*'];
    
    public function level() {
        return $this->belongsTo('App\OrganizationStructure', 'organization_structure_id', 'id');
    }

    public function organizationStructureInstance() {
        return $this->belongsTo('App\OrganizationStructureInstance');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'user_positions', 'position_id', 'user_id')->withTimestamps()->whereNull('user_positions.deleted_at');
    }
}
