<?php

namespace App\Libraries;

class SOP extends Document {

    
    protected  $doc;

    public static function nambahStatis() {
        return 'nambahStatis';
    }

    public function nambahBiasa() {
        $this->type .= 'n a m b a h';
        return $this;
    }

    public function nambahBiasaBanget() {
        $this->type .= 'b_i_a_s_a';
        return $this;
    }

    public function __construct() {
        $this->type = 'SOP';
    }
}