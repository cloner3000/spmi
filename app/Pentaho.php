<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pentaho extends Model
{
    protected $connection = 'mysql_pentaho';

    protected $fillable = ['*'];
}
